const htmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  mode: "development",
  plugins: [
    new htmlWebpackPlugin({template: "./src/index.html"}),
	new MiniCssExtractPlugin({
	  filename: "[name].css"
	}),
  	new CopyWebpackPlugin([
	  {from: 'src/img',
		to:'img'
	  }
	])],
  module: {
	rules: [
	  {
		test: /\.s?css$/,
		use: [
		  //"style-loader",
		  MiniCssExtractPlugin.loader,
		  "css-loader",
		  {
			loader: "sass-loader",
			options: {
			  implementation: require("sass")
			}
		  }
		]
	  },

	]
  }
};